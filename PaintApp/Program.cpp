#include <ConsoleThickWrapper.hpp>
#include <App.hpp>

//Booleans to decide if the program is done and when a mouse click has ended
bool done = false;
bool LeftMousePressed = false;

//Colour objects for drawing
ThickConsole::COLOUR drawColours[3];
int currentColour = 0;
enum DrawColour {
	Red,
	Green,
	Blue
};	

//=======================================================
// Event Handlers
//=======================================================
BOOL CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		done = true;
		return TRUE;
	}

	return FALSE;
}

void ProcessKeyEvent(KEY_EVENT_RECORD const& ker, ThickConsole* ThickConsoleObj) {
	auto ks = ker.dwControlKeyState;
	if (ker.uChar.AsciiChar == 'c')
	{
		//cout << "C PRESSED";
		ThickConsoleObj->SetScreenColour(ThickConsoleObj->SCREEN_WHITE);
	}
}


void MouseEventProc(MOUSE_EVENT_RECORD const& mer, ThickConsole* ThickConsoleObj) {
#if !defined(MOUSE_HWHEELED)
#define MOUSE_HWHEELED 0x0008
#endif
	auto bufferLoc = mer.dwMousePosition;

	switch (mer.dwEventFlags) {
	case 0:	// button pressed or released
	{

		auto mask = mer.dwButtonState;
		if (mask&FROM_LEFT_1ST_BUTTON_PRESSED)
		{
			//cout << " left pressed";
			ThickConsoleObj->SetCellColour(drawColours[currentColour], COORD{ bufferLoc.X, bufferLoc.Y });
			LeftMousePressed = true;
		}
		if (mask&RIGHTMOST_BUTTON_PRESSED)
			if (currentColour < Blue)
				++currentColour;
			else
				currentColour = Red;
		if (!mask)
			LeftMousePressed = false;
	}
	break;
	default:
		//cout << " unknown";
		break;
	}
	if(LeftMousePressed)
		ThickConsoleObj->SetCellColour(drawColours[currentColour], COORD{ bufferLoc.X, bufferLoc.Y });
}

void ResizeEventProc(WINDOW_BUFFER_SIZE_RECORD wbsr, ThickConsole* ThickConsoleObj) {/*Save previous drawing when resized?*/}

//=======================================================
// Main
//=======================================================
class PaintProgram : public App {
	int execute() override {
		SHORT WIDTH = 70;
		SHORT HEIGHT = 50;

		ThickConsole ThickConsoleClass;

		drawColours[0] = ThickConsoleClass.SCREEN_RED; drawColours[1] = ThickConsoleClass.SCREEN_GREEN; drawColours[2] = ThickConsoleClass.SCREEN_BLUE;

		// Install a control handler to trap ^C
		if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE)) {
			cerr << "ERROR: failed to install control handler." << endl;
			return EXIT_FAILURE;
		}

		//Save state and set default colour scheme so output matches
		ThickConsoleClass.SaveConsoleState();

		ThickConsoleClass.SetDefaultScreenColour(ThickConsoleClass.FOREGROUND_BLACK | ThickConsoleClass.BACKGROUND_WHITE);

		cout << "Terminate program with Ctrl-C\n";

		ThickConsoleClass.ShowCursor(false);


		ThickConsoleClass.ResizeWindow(70, 50);
		// enable input events
		ThickConsoleClass.SetMode(ThickConsoleClass.ENABLE_CONSOLE_INPUT_MODE);
		// read console input
		vector<INPUT_RECORD> inBuffer(128);
		while (!done) {
			DWORD numEvents = ThickConsoleClass.ReadInputs(&inBuffer);
			if (numEvents > 0)
			{
				for (size_t iEvent = 0; iEvent < numEvents; ++iEvent) {
					switch (inBuffer[iEvent].EventType) {
					case MOUSE_EVENT:
						MouseEventProc(inBuffer[iEvent].Event.MouseEvent, &ThickConsoleClass);
						break;
					case KEY_EVENT:
						ProcessKeyEvent(inBuffer[iEvent].Event.KeyEvent, &ThickConsoleClass);
						break;
					case WINDOW_BUFFER_SIZE_EVENT: // window resizing
						ResizeEventProc(inBuffer[iEvent].Event.WindowBufferSizeEvent, &ThickConsoleClass);
						break;
					}
				}
			}
		}

		ThickConsoleClass.RestoreConsoleState();
		cout << "Console restored..." << endl;
		return EXIT_SUCCESS;
	}
}PaintApp;