#pragma once
#include <Windows.h>
#undef min

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

class XError {
public:
	using id_type = decltype(GetLastError());
	using file_type = char const*;
	using string_type = std::string;
private:
	id_type code_;
	int line_;
	file_type file_;
public:
	XError(int line, file_type file);
	auto code() const->id_type;
	auto file() const->file_type;
	auto line() const -> int;

	string_type msg() const;
};

class Console
{
	//Variables to save state
	// System Data
	HANDLE hConsoleInput, hConsoleOutput;
	CONSOLE_SCREEN_BUFFER_INFO	CSBI;
	CONSOLE_CURSOR_INFO			CCI;
	vector<CHAR_INFO>			Buffer;
	COORD						BufferCoord;
	DWORD						ConsoleMode;

public:

	Console();

	//===============================
	//GET
	//===============================
	vector<char>				CGetConsoleTitle();

	CONSOLE_SCREEN_BUFFER_INFO	CGetScreenBufferInfo();

	vector<CHAR_INFO>			CGetOutput(COORD bufferSize, COORD startIndex, SMALL_RECT areaToRead);

	CONSOLE_CURSOR_INFO			CGetCursorInfo();

	DWORD						CGetConsoleMode();

	DWORD						CReadInput(vector<INPUT_RECORD>* record);

	//===============================
	//SET
	//===============================
	void						CSetTitle(vector<char> origTitle);

	void						CResizeWindow(CONSOLE_SCREEN_BUFFER_INFO origCSBI);

	void						CResizeWindow(int width, int height);

	void						CSetOutput( vector<CHAR_INFO> outputChars ,COORD bufferSize, COORD startIndex, SMALL_RECT areaToWrite);
		
	void						CSetCursorInfo(CONSOLE_CURSOR_INFO cursorInfo);

	void						CSetCursorPos(COORD cursorPos);

	bool						CSetConsoleMode(DWORD oldConsoleMode);

	void						CSetConsoleAttributeColour(WORD colour);

	void						CSetCellAttribute(WORD colour, COORD coord);
	
	void						CSetTextAttribute(WORD colour);
};