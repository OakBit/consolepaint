#pragma once
#include "ConsoleThinWrapper.hpp"
#if defined(_DEBUG) && defined(_DLL)
#pragma comment (lib,"ConsolePaintLib-mt-gd.lib")
#elif defined(_DEBUG) && !defined(_DLL)
#pragma comment (lib,"ConsolePaintLib-mt-sgd.lib")
#elif !defined(_DEBUG) && defined(_DLL)
#pragma comment (lib,"ConsolePaintLib-mt.lib")
#elif !defined(_DEBUG) && !defined(_DLL)
#pragma comment (lib,"ConsolePaintLib-mt-s.lib")
#endif


class ThickConsole
{
	Console consoleClass;

	vector<char> origTitle;
	CONSOLE_SCREEN_BUFFER_INFO originalCSBI;
	SMALL_RECT bufferRect{ 0 };
	vector<CHAR_INFO> origConsoleOutput;
	CONSOLE_CURSOR_INFO CCInfo;
	DWORD CMode;

	static ThickConsole* thisApp;
public:

	using COLOUR = WORD;
	COLOUR const BACKGROUND_BLACK = 0;
	COLOUR const BACKGROUND_WHITE = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
	COLOUR const FOREGROUND_BLACK = 0;
	COLOUR const FOREGROUND_WHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY;

	COLOUR const SCREEN_RED = FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_INTENSITY;
	COLOUR const SCREEN_BLUE = FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
	COLOUR const SCREEN_GREEN = FOREGROUND_GREEN | FOREGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_INTENSITY;

	COLOUR const SCREEN_WHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;

	DWORD const ENABLE_CONSOLE_INPUT_MODE = ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_EXTENDED_FLAGS | ENABLE_MOUSE_INPUT;


	ThickConsole();


	void SaveConsoleState();
	void RestoreConsoleState();
	void ShowCursor(bool bShow);
	void SetDefaultScreenColour(COLOUR colour);

	void SetMode(DWORD cMode);
	void SetScreenColour(COLOUR c);

	DWORD ReadInputs(vector<INPUT_RECORD>* records);
	void ResizeWindow(int width, int height);
	void SetCellColour(COLOUR colour, COORD coord);

};