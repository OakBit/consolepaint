
#include <ConsoleThickWrapper.hpp>
using namespace std;

	ThickConsole* ThickConsole::thisApp = nullptr;

	ThickConsole::ThickConsole(){
		if (thisApp)
			throw "Error: Already initialized!";
		thisApp = this;
	}
	//SAVE
	//Returns the index of the state object saved
	void ThickConsole::SaveConsoleState()
	{
		origTitle = consoleClass.CGetConsoleTitle();
		// Get the old window/buffer size
		originalCSBI = consoleClass.CGetScreenBufferInfo();
		// Save the desktop
		bufferRect.Right = originalCSBI.dwSize.X - 1;
		bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
		origConsoleOutput = consoleClass.CGetOutput(originalCSBI.dwSize, COORD{ 0 }, bufferRect);

		// Save the cursor
		CCInfo = consoleClass.CGetCursorInfo();
		//Save the console mode
		CMode = consoleClass.CGetConsoleMode();
	}


	void ThickConsole::RestoreConsoleState()
	{
		//RESTORE
		consoleClass.CResizeWindow(originalCSBI);

		consoleClass.CSetOutput(origConsoleOutput, originalCSBI.dwSize, COORD{ 0 }, bufferRect);

		consoleClass.CSetTitle(origTitle);

		//Reset default colours
		consoleClass.CSetTextAttribute(originalCSBI.wAttributes);
		// Restore the cursor
		consoleClass.CSetCursorInfo(CCInfo);
		consoleClass.CSetCursorPos(originalCSBI.dwCursorPosition);
		//Restore console mode
		consoleClass.CSetConsoleMode(CMode);
	}

	void ThickConsole::ShowCursor(bool bShow)
	{
		CONSOLE_CURSOR_INFO cciTemp = CONSOLE_CURSOR_INFO();
		cciTemp.dwSize = 25; //percentage of cell filled by cursor, needs to be > 0 and < 100
		cciTemp.bVisible = bShow;
		consoleClass.CSetCursorInfo(cciTemp);
	}

	void ThickConsole::SetDefaultScreenColour(COLOUR colour)
	{
		consoleClass.CSetTextAttribute(colour);
	}


	void ThickConsole::SetScreenColour(COLOUR colour)
	{
		consoleClass.CSetConsoleAttributeColour(colour);
	}

	void ThickConsole::SetMode(DWORD cMode)
	{
		consoleClass.CSetConsoleMode(cMode);
	}

	DWORD ThickConsole::ReadInputs(vector<INPUT_RECORD>* records)
	{
		return consoleClass.CReadInput(records);
	}

	void ThickConsole::ResizeWindow(int width, int height)
	{
		consoleClass.CResizeWindow(width, height);
	}

	void ThickConsole::SetCellColour(COLOUR colour, COORD coord)
	{
		consoleClass.CSetCellAttribute(colour, coord);
	}