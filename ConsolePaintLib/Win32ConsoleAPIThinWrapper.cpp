#include <ConsoleThinWrapper.hpp>

// Colours
WORD const FOREGROUND_BLACK = 0;
WORD const FOREGROUND_WHITE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
WORD const BACKGROUND_BLACK = 0;
WORD const BACKGROUND_WHITE = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE;

#pragma region XError Class and Utilities
/* ErrorDescription converts Win32 error codes into a human readable string. */
string ErrorDescription(DWORD dwMessageID) {
	char* msg;
	auto c = FormatMessageA(
		/* flags */ FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		/* source */ NULL,
		/* message ID */ dwMessageID,
		/* language */		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		/* buffer */	(LPSTR)&msg,
		/* size */		0,
		/* args */		NULL);
	string strMsg = (c == 0)
		? "unknown"
		: msg;
	LocalFree(msg);
	return strMsg;
}


	XError::id_type code_;
	int line_;
	XError::file_type file_;

	XError::XError(int line, file_type file) :code_(GetLastError()), line_(line), file_(file) {}
	auto XError::code() const -> id_type { return code_; }
	auto XError::file() const -> file_type { return file_; }
	auto XError::line() const -> int { return line_; }

	XError::string_type XError::msg() const {
		ostringstream oss;
		oss << "Error: " << code() << "\n" << ErrorDescription(code()) << "\nIn: " << file() << "\nLine: " << line() << "\n";
		return oss.str();
	}


#define THROW_CONSOLE_ERROR() throw XError(__LINE__, __FILE__)
#define THROW_IF_CONSOLE_ERROR(res) if(!res) throw XError(__LINE__, __FILE__)
#pragma endregion

#pragma region ConsoleClass



	Console::Console()
	{
		hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
		hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	}

	//===============================
	//GET
	//===============================
	vector<char> Console::CGetConsoleTitle()
	{
		vector<char> originalTitle = vector<char>(64 * 1024);
		originalTitle.resize(GetConsoleTitleA(originalTitle.data(), (DWORD)originalTitle.size()) + 1);
		originalTitle.shrink_to_fit();
		return originalTitle;
	}

	CONSOLE_SCREEN_BUFFER_INFO Console::CGetScreenBufferInfo()
	{
		CONSOLE_SCREEN_BUFFER_INFO	CSBI;
		THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &CSBI));
		return CSBI;
	}

	vector<CHAR_INFO> Console::CGetOutput(COORD bufferSize, COORD startIndex, SMALL_RECT areaToRead)
	{
		vector<CHAR_INFO> bufferData(bufferSize.X*bufferSize.Y);

		THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(hConsoleOutput, bufferData.data(), bufferSize, startIndex, &areaToRead));

		return bufferData;
	}

	CONSOLE_CURSOR_INFO Console::CGetCursorInfo()
	{
		THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &CCI));
		return CCI;
	}

	DWORD Console::CGetConsoleMode()
	{
		DWORD oldConsoleMode;
		GetConsoleMode(hConsoleInput, &oldConsoleMode);
		return oldConsoleMode;
	}

	DWORD Console::CReadInput(vector<INPUT_RECORD>* records)
	{
		DWORD numEvents;
		ReadConsoleInput(hConsoleInput, records->data(), (DWORD)records->size(), &numEvents);
		return numEvents;
	}

	//===============================
	//SET
	//===============================
	void Console::CSetTitle(vector<char> origTitle)
	{
		THROW_IF_CONSOLE_ERROR(SetConsoleTitleA(origTitle.data()));
	}

	void Console::CResizeWindow(CONSOLE_SCREEN_BUFFER_INFO origCSBI)
	{
		// Restore the original settings/size
		SMALL_RECT sr{ 0 };
		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
		THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, origCSBI.dwSize));
		THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &origCSBI.srWindow));
	}

	void Console::CResizeWindow(int width, int height)
	{
		// Resize the console
		SMALL_RECT sr{ 0 };
		SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr);
		COORD bufferSize;
		bufferSize.X = width;
		bufferSize.Y = height;
		SetConsoleScreenBufferSize(hConsoleOutput, bufferSize);
		CONSOLE_SCREEN_BUFFER_INFO sbi;
		GetConsoleScreenBufferInfo(hConsoleOutput, &sbi);
		width = min((SHORT)width, sbi.dwMaximumWindowSize.X);
		height = min((SHORT)height, sbi.dwMaximumWindowSize.Y);
		sr.Top = sr.Left = 0;
		sr.Right = width - 1;
		sr.Bottom = height - 1;
		SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr);
	}

	void Console::CSetOutput( vector<CHAR_INFO> outputChars, COORD bufferSize, COORD startIndex, SMALL_RECT areaToWrite)
	{
		THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, outputChars.data(), bufferSize, startIndex, &areaToWrite));
	}

	void Console::CSetCursorInfo(CONSOLE_CURSOR_INFO cursorInfo)
	{
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &cursorInfo));
	}

	void Console::CSetCursorPos(COORD cursorPos)
	{
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, cursorPos));
	}

	bool Console::CSetConsoleMode(DWORD oldConsoleMode)
	{
		return SetConsoleMode(hConsoleInput, oldConsoleMode);
	}

	void Console::CSetConsoleAttributeColour(WORD colour)
	{
		//Sets the entire background colour
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

		DWORD charsWritten;
		DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
		COORD cursorHomeCoord{ 0, 0 };
		THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, ' ', consoleSize, cursorHomeCoord, &charsWritten));
		THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, colour, consoleSize, cursorHomeCoord, &charsWritten));
	}

	void Console::CSetCellAttribute(WORD colour,COORD coord)
	{
		//Sets a single cell colour
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));

		DWORD charsWritten = 1;
		DWORD consoleSize = csbi.dwSize.X * csbi.dwSize.Y;
		THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, colour, charsWritten, coord, &charsWritten));
	}

	void Console::CSetTextAttribute(WORD colour)
	{
		THROW_IF_CONSOLE_ERROR(SetConsoleTextAttribute(hConsoleOutput, colour));
	}

#pragma endregion